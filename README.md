# Power monitor for Mikrotik

## Purpose of use

This document describes how to configure your Mikrotik router to monitor internet (power) availability on remote location.


## Telegram Bot

* Open [@BotFather](https://t.me/BotFather) and register a new bot.
* Create new channel and add there your new bot with posting permission.
* Open `telegram.org/bot5758577912:ZZZzZzzZZZz-yYYYYyYYyy-XXXxxXXX/getUpdates` and find out your `chatId`. Post something in your channel if `getUpdates` is empty.


## Mikrotik scripts

* Add fixed IPs to NetWatch. Better results can be reached with multiple IPs on different providers.
```
/tool netwatch
add comment=nw1 host=1.2.3.4 interval=30s
add comment=nw1 host=5.6.7.8 interval=30s
add comment=nw1 host=9.10.11.12 interval=30s
```

* Add script
```
/system script
add dont-require-permissions=no name=TGmonitor owner=root policy=read,write,test source=":global NetwatchName;\
    \n:global BotToken;\
    \n:global ChatId;\
    \n\
    \n:local countUp 0;\
    \n:local status;\
    \n:foreach monitor in=[/tool netwatch find where comment=\"\$NetwatchName\"] do={\
    \n  :set \$status [/tool netwatch get \$monitor status]\
    \n  :if (\$status = \"up\") do={\
    \n    :set countUp (\$countUp + 1);\
    \n  }\
    \n}\
    \n:local downReported [/file find where name=\"TGmonitor/\$NetwatchName.down\"]\
    \n:if ((\$countUp > 0) && ([:len \$downReported] > 0)) do={\
    \n  #status is up and was reported down\
    \n  /tool fetch url=\"https://api.telegram.org/bot\$BotToken/sendMessage\?chat_id=\$ChatId&text=\$NetwatchName%3A%20Power%20status%20changed%20to%20UP%21\" output=none\
    \n  :foreach file in \$downReported do={\
    \n    /file remove \$file\
    \n  }\
    \n  :log info \"Reported \$NetwatchName is UP!\"\
    \n}\
    \n:if ((\$countUp = 0) && ([:len \$downReported] = 0)) do={\
    \n  #status is down and was not reported down\
    \n  /tool fetch url=\"https://api.telegram.org/bot\$BotToken/sendMessage\?chat_id=\$ChatId&text=\$NetwatchName%3A%20Power%20status%20changed%20to%20DOWN%21\" output=file dst-path=\"TGmonitor/\$NetwatchName.down\"\
    \n  :log info \"Reported \$NetwatchName is DOWN!\"\
    \n}\
    \n"
```

* Add scheduler task
```
/system scheduler
add interval=30s name=TGmonitor_nw1 on-event="{\
    \n  :global NetwatchName nw1;\
    \n  :global BotToken \"5758577912:ZZZzZzzZZZz-yYYYYyYYyy-XXXxxXXX\";\
    \n  :global ChatId -1001879913493;\
    \n  /system script run TGmonitor;\
    \n}" policy=read,write,test start-date=jan/12/2023 start-time=06:30:00
```
